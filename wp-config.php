<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'health2body_new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tDCNY;,/nz-EW{?L?i|azbOSRqtEpw:V}iK0[S}fcg7(|kicPg&S+$phc.kh|B;D');
define('SECURE_AUTH_KEY',  'BD/-Z.]w&IZ }4>BO:yct<Z+W[J{gS$~/3<PA)0RrOC?>{v]eQo$$2b=BMF^CEXF');
define('LOGGED_IN_KEY',    'JJ|fiUTwW#<;!%HW Bs,)z)*AyHEz(+@pfqXWmH$v_$/zz<5Tp2`JjmEBOw6U1VY');
define('NONCE_KEY',        '+p`-@$5*.W;UPFAg:WjYYRiIr_g=39ze$B 4g17cU[JUb6UyEO~-n:[;zwg}pY$y');
define('AUTH_SALT',        'rPg!xC;LXO[xPuu,nVMp}]pS[IFIGVnHS{,H|tbpBV4aJNpdu7O<:9oEnO%k]YbL');
define('SECURE_AUTH_SALT', '{R7C6-8v:&JLgjS|v^MY+Q&g:P/0P]3>d=NIz4vaiUPu0i[+d%FuENe`}-<TYIqQ');
define('LOGGED_IN_SALT',   '5%5(r/|^>OTYdWgzL,|zl=6RT8^x=<}RJ:l*(+yxl]>jT.f`CMDz|bxpzk1|65$p');
define('NONCE_SALT',       ',GzR,NEj]nkSqV/cZ,JNJoMPGCYhYduccTWBv:#N0*V/7j-kB!u;e/)4E[FGo1_I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
